let text = `GDL Group was founded in 2004. We are a professional IT Solution
provider
across various geographies. Our business has grown significantly
since our inception.
Our risk-sharing approach and our commitment to our client have
led us to significantly
reduce their total cost of ownership (TCO) as well as transactional
Sustaining vital connections with employees, suppliers and
customers is the lifeblood of
your business and it depends on an IT infrastructure that
encourages collaboration and
assures optimum performance at every link in your value chain,
around-the-clock.
GDL Group is about optimizing investments, transforming
capabilities and achieving
greater business value from IT costs.`;

let textId = (document.getElementById("text").innerText = text);

function WordCount(str, char) {
  return str.toLowerCase().split(char).length - 1;
}

function getWord(char) {
  let x = document.getElementById(char);
  let result = WordCount(text, char.toLowerCase());
  x.innerText = `${char} = ${result}`;
}

getWord("A");
getWord("E");
getWord("I");
getWord("O");
getWord("U");

function countGrade() {
  let fname = document.getElementById("fname").value;
  let lname = document.getElementById("lname").value;
  let nickname = document.getElementById("nickname").value;
  let age = document.getElementById("age").value;

  let cd_a = document.getElementById("cd_a").value;
  let g_a = document.getElementById("g_a").value;

  let cd_b = document.getElementById("cd_b").value;
  let g_b = document.getElementById("g_b").value;

  let cd_c = document.getElementById("cd_c").value;
  let g_c = document.getElementById("g_c").value;

  let cd_d = document.getElementById("cd_d").value;
  let g_d = document.getElementById("g_d").value;

  let sub_a = cd_a * g_a;
  let sub_b = cd_b * g_b;
  let sub_c = cd_c * g_c;
  let sub_d = cd_d * g_d;

  let result_mul_sub = sub_a + sub_b + sub_c + sub_d;

  let result_credit =
    parseInt(cd_a) + parseInt(cd_b) + parseInt(cd_c) + parseInt(cd_d);

  let gpa = result_mul_sub / result_credit;

  let div_result = document.getElementById("results");

  div_result.innerHTML = `
    <p>name : ${fname} ${lname}</p>
    <p>nickname : ${nickname} </p>
    <p>Age : ${age} </p>
    <p>GPA : ${gpa} </p>
    `;
}

function resetInput() {
  document.getElementById("fname").value = "";
  document.getElementById("lname").value = "";
  document.getElementById("nickname").value = ""; 
  document.getElementById("age").value = "";

  document.getElementById("cd_a").value = "";
  document.getElementById("g_a").value = "";

  document.getElementById("cd_b").value = "";
  document.getElementById("g_b").value = "";

  document.getElementById("cd_c").value = "";
  document.getElementById("g_c").value = "";

  document.getElementById("cd_d").value = "";
  document.getElementById("g_d").value = "";

  document.getElementById("results").innerHTML = "";
}
